var express = require('express');
var router = express.Router();
const admin = require('firebase-admin');
const firebase = require('firebase');
const db = require('../db/dbconfig');


/* GET home page. */
router.get('/', function (req, res, next) {
  res.redirect('/login');
});

router.get('/login', (req, res, next) => {
  res.render('index', {
    title: 'Express',
    ch: 0
  });
});

router.get('/forget_password', (req, res, next) => {
  res.render('forget', {
    ch: 0
  });
});

router.get('/register_account', (req, res, next) => {
  res.render('create', {
    ch: 0
  });
});

router.post('/create_acc', function (req, res, next) {
  admin.auth().createUser({
      email: req.body.email,
      emailVerified: true,
      phoneNumber: '+91' + req.body.phno,
      password: req.body.pass,
      displayName: req.body.name,
      disabled: false,
    })
    .then((create) => {
      res.redirect('/');
    })
    .catch((error) => {
      con
      res.render('create', {
        ch: 1,
        message: error.message
      });
    })
});

router.post('/forget_pass', (req, res, next) => {
  console.log(req.body);
  firebase.auth().sendPasswordResetEmail(req.body.email)
    .then(() => {
      res.render('forget', {
        ch: 1,
        message: "Sent Email!!"
      })
    })
    .catch((error) => {
      res.render('forget', {
        ch: 1,
        message: error.message
      })
    })
});

router.post('/log_in', (req, res, next) => {
  firebase.auth().signInWithEmailAndPassword(req.body.email, req.body.password)
    .then((userCredential) => {
      // Signed in
      req.session.user_mail = req.body.email;
      console.log(req.session)
      res.redirect('/dashboard')
      // ...
    })
    .catch((error) => {
      res.render('index', {
        ch: 1,
        message: error.message
      });
    });
});

router.get('/dashboard', (req, res, next) => {
  console.log(req.session)
  if (req.session.user_mail) {
    res.redirect('/dashboard/all');
  } else {
    res.redirect('/');
  }
})

router.get('/dashboard/all', async function (req, res, next) {
  if (req.session.user_mail) {
    data = []
    const reviewref = db.collection('Review');
    let snapshot = await reviewref.get()
    snapshot.forEach(doc => {
      data.push(doc.data())
    })
    console.log(data)
    return res.render('dashboard', {
      ch: 0,
      data: data
    });
  } else {
    res.redirect('/');
  }

});

router.get('/dashboard/pos', async function (req, res, next) {
  if (req.session.user_mail) {
    data = []
    const reviewref = db.collection('Review');
    let snapshot = await reviewref.where('type', '==', 'Positive').get()
    snapshot.forEach(doc => {
      data.push(doc.data())
    })
    console.log(data)
    return res.render('dashboard', {
      ch: 0,
      data: data
    });
  } else {
    res.redirect('/');
  }

});

router.get('/dashboard/neg', async function (req, res, next) {
  if (req.session.user_mail) {
    data = []
    const reviewref = db.collection('Review');
    let snapshot = await reviewref.where('type', '==', 'Negative').get()
    snapshot.forEach(doc => {
      data.push(doc.data())
    })
    console.log(data)
    return res.render('dashboard', {
      ch: 0,
      data: data
    });
  } else {
    res.redirect('/');
  }

});

router.get('/dashboard/neu', async function (req, res, next) {
  if (req.session.user_mail) {
    data = []
    const reviewref = db.collection('Review');
    let snapshot = await reviewref.where('type', '==', 'Neutral').get()
    snapshot.forEach(doc => {
      data.push(doc.data())
    })
    console.log(data)
    return res.render('dashboard', {
      ch: 0,
      data: data
    });
  } else {
    res.redirect('/');
  }

});

router.post('/dashboard/dis', (req, res, next) => {
  if (req.body.val == 'all') {
    res.redirect('/dashboard/all');
  } else if (req.body.val == 'pos') {
    res.redirect('/dashboard/pos');
  } else if (req.body.val == 'neg') {
    res.redirect('/dashboard/neg');
  } else if (req.body.val == 'neu') {
    res.redirect('/dashboard/neu');
  }
});

router.get('/logout', (req, res, next) => {
  firebase.auth().signOut().then(function () {
      req.session.destroy();
      res.redirect('/');
    })
    .catch(function (err) {
      console.log(err);
    });
});

module.exports = router;