var express = require('express');
var router = express.Router();
const axios = require('axios');

const db = require('../db/dbconfig');

router.post('/',(req,res,next)=>{
    console.log(req.body)
    data = req.body.item_review
    axios.post('https://flask-api-final-year-project.herokuapp.com/review',
    {
        "review": data
    })
    .then((ress)=>{
        console.log(ress.data)
        database_val = {
            "id":req.body.id,
            "item_name":req.body.item_name,
            "review": data,
            "review_predict": ress.data.review_predict.review,
            "type": ress.data.review_predict.type,
            "review_summary": ress.data.review_summary
        }

        db.collection('Review').add(database_val)
        .then(()=> {
            res.status(200).send({message: "Recieved"})
        })
        .catch(()=> {
            res.status(400).send({message:"failed"})
        })
    })
    .catch((error)=> {
        console.log(error)
    })
});

module.exports = router;
