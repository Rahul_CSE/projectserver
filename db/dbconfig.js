const admin = require('firebase-admin');

admin.initializeApp({
    credential: admin.credential.cert({
        "type": "service_account",
        "project_id": "finalyearproject-8bd84",
        "private_key_id": "7b52ee264422c3d8753621819a94fc8c8ebf5322",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCaYGG00uwy6yY3\nAa+tSjKa+Sf43Jh7L8x62Hlncq17JCGUXKNUHDKxSPTmVcZHKE9V2SzVFdxDlFEb\nVbM5Cx5Tjaujys/7IQLDn7keSKcO1lh2M8RhJF+8gg2TV/DrDcYjqSHkiz+iNnnR\n8TpkUSaME41MmInkisczMwpvRu5vuv2Ms1uzTdl4ulJVdUzLAzdHaTpqNzZ+/2yG\n8ITPMGUZmhR/CCoCMkhBzCktohx1PxLK2492KxoHhuRyvzrgXJ3jHL//56zsujKK\nNEFXWe5j2jqJvnaPabmdoEYzg84y+8wSBk+0BWeGwIPm8ijyQgo9RyLCBALCgAmq\nLZzLtn5xAgMBAAECggEAItWEtoyxh3vQD7Fttqh7asS15E3Cimlqtn2KR3F0+8hX\nW2bb4+rI9YAE4sSQDRZncHOkziA0p3scIhLgcgq0OR5KkJb+/yefvAyadN7n800i\ngOmFcnYGtD6UWZ8KWb+t6fYME8nn/e5U5q6MAn7dN9Zhl9sDonI+KKtnu1xCCSvK\nuM8XW/w82zWIC3f7+xdGVYdhfYdt5LumicWKCDLX3qWX/cOSmO5hqwgE1vZz4lL1\nDU/9UxldqtNOBWtOO+WN3tlkq8fXcTgniLWaTL72JVhISt0kEhbHGdBYTcj8QMgG\naSt6r283Iwtd37zrAYdybYYKn8tZZGkMU2W8umB60QKBgQDJx42H52BcJe4NhH6P\nXQb0qUAax84NobshK/WFba242fkgb7dzr2LO6F4ZsXtbs2GzPlogUnELae5PA5Am\nVrlL5WvQpdgfhaoqT/CK2VCbAoZkbLVjfi8OS8dbYE3N3Ybi+0shnBvBQ2RhmcgA\nW6bMjykn4v8c9qqun4TV/l7YowKBgQDD2/ejRYeLvGzK26CJ/xE3hk+AoaIW25QC\nI3HBuPO/0bYHC059dt7NLlSC3YXHC4/USrRaYblZ84M905ERgTGWvp8T9lauRpfy\nm6xKXBWZ75WpDKsyPbRhxVuvYCwQXj77fMQdfksr786C6ga6B8eyeu/Kb9ihG+h0\nYLINo1vZ2wKBgAfTk6vHz7cDSZ6AmyAYbaKjxfbD9TwWH9oDiHxqaqiW29DkRGta\nTCzWWoAIoE5IQOvg6UIDsJrOuw2Zh8fU6U8GUQ5AwIdIefc2LpBUKYo5qNjQVqDL\nTivxyIPE0n+/rfGz6ZAeK/1tDcGzY0yQQYIBs4j2hwqqjqhDS7W5LhidAoGALvuQ\nPulJ55pOmqbursux7Jjab2fWgxcvPQgVnLvqNMRwnuX2cSBXt+56NV5AXgwG94zo\nEJpKpSpF/EnJDeLo61lUvawlBO9IOdlgDgJi6CdRxliUpJuqrp1yLfKq/04sW5WF\nUfHexG5lIoBTFsMRA158MRp9Y8BXFbuwWpcqTGsCgYAPBFT7qph65VTxcgV4u/30\n5fR3nSzRYL0rK4jWHwazCLbbZ/Pk8V0VmeuZZvT/ivttvO67PoP43fL9twIftZZn\nRw8MjRG1gvmEXBDaerOOAoHj782RVjHF7KE8gsO5le3D3zy9D7sOF5/Ed7L0Etak\nPFhfKnUQTW1J3pGEvHh8xg==\n-----END PRIVATE KEY-----\n",
        "client_email": "firebase-adminsdk-dfylt@finalyearproject-8bd84.iam.gserviceaccount.com",
        "client_id": "115566310140158229628",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-dfylt%40finalyearproject-8bd84.iam.gserviceaccount.com"
      }),
    databaseURL: "https://finalyearproject-8bd84.firebaseio.com",
    storageBucket: "finalyearproject-8bd84.appspot.com"
  });


const db = admin.firestore();

module.exports = db;