var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var dataentryRouter = require('./routes/db');
const firebase = require('firebase');
var session = require('express-session');
var FileStore = require('session-file-store')(session);
var app = express();

const firebaseConfig = {
  apiKey: "AIzaSyDOfP91emIOwET8Y5uOEIZpDcGstV0AT6s",
  authDomain: "finalyearproject-8bd84.firebaseapp.com",
  projectId: "finalyearproject-8bd84",
  storageBucket: "finalyearproject-8bd84.appspot.com",
  messagingSenderId: "619829622127",
  appId: "1:619829622127:web:0c3e475fd7f421cb32249a",
  measurementId: "G-0FR4724LF0"
};


firebase.initializeApp(firebaseConfig);

app.set('trust proxy', 1) // trust first proxy
app.use(session({
  secret: 'keyboard cat',
  store: new FileStore(),
  resave: false,
  saveUninitialized: true
}))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/reviews', dataentryRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
